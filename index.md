---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I am a professor of computer science at the University of British Columbia.


## Research Interests

Computers give us access to and control over data and machines, but they have also taken away the handles our bodies evolved to use. My research's central goal is to restore physicality to computer interaction, take it away from the desktop and embed it into the world at the best point of use. I use haptic (touch) force feedback as part of a multimodal HCI design toolbox, and apply design techniques to real problems and contexts to better understand its ideal uses and deployment. The applications I've found most exciting to date are those that require continuous and/or expressive control or navigation - for example, manipulating streaming media, drawing and sculpting, controlling musical instruments, affective displays, and computer-mediated interpersonal affective communication. Other promising areas are those where other senses are overutilized (driving), or a system is being monitored with low attention (the pager of the future).

## Selected Publications

{% bibliography --file selected.bib %}
